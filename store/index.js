import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		imageUrl:'https://www.wangmiaojulebu.com'
	},
    mutations: {},
    actions: {}
})
export default store