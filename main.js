import Vue from 'vue'
import App from './App'
//luch-request
import Request from 'common/js_sdk/luch-request/luch-request/index.js' // 下载的插件
global.$http = new Request();
global.$http.interceptors.request.use((config) => { // 可使用async await 做异步操作
	config.header = {
		...config.header,
		a: 0 // 演示拦截器header加参
	}
	if(config.url=="/api/home/picterUp"){
		 return config
	}
	if (config.data.token == '') { // 如果token不存在，return Promise.reject(config) 会取消本次请求
		config.header = {
			...config.header,
			a: 1 // 演示拦截器header加参
		}
		uni.showToast({
			title: '请先登录',
			icon: "none",
			mask: true,
			duration: 2000
		})
		setTimeout(function() {
			uni.navigateTo({
				url: '../../logins/login/login'
			})
		}, 1000)
		return Promise.reject(config)
	} else {
		return config
	}
}, config => { // 可使用async await 做异步操作
	return Promise.reject(config)
})
global.$http.interceptors.response.use((response) => {
	/* 对响应成功做点什么 可使用async await 做异步操作*/
	// console.log(response)
	if (response.data.code == 1) {
		uni.showToast({
			title: response.data.msg,
			icon: "none",
			mask: true,
			duration: 2000
		})
	}
	if (response.data.code == 0||response.data.code == 200) {
		return response
	}
	if (response.data.code == 10000) {
		uni.showToast({
			title: response.data.msg,
			icon: "none",
			mask: true,
			duration: 2000
		})
		setTimeout(function() {
			uni.navigateTo({
				url: '../../logins/login/login'
			})
		}, 1000)
	}
}, (response) => {
	// console.log(response)
	/*  对响应错误做点什么 （statusCode !== 200）*/
	if (response.header.a == 0) {
		uni.showToast({
			title: "接口访问失败",
			icon: "none",
			mask: true,
			duration: 2000
		})
		return Promise.reject(response)
	}
})
//uview-ui
import uView from "uview-ui";
Vue.use(uView);
//配置全局时间格式
import formatTime from 'store/filters.js'
Vue.prototype.formatTime=formatTime
//引入vuex
import store from './store'
//把vuex定义成全局组件
Vue.prototype.$store = store
// 阻止启动生产消息
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App,
	//挂载
	store
})
app.$mount()
