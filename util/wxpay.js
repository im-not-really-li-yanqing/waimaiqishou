const wx = require('jweixin-module');
export function wxPay(data, callback, errCallback) {

	wx.config({
		debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
		appId: data.appId, // 必填，公众号的唯一标识
		timestamp: data.timeStamp, // 必填，生成签名的时间戳
		nonceStr: data.nonceStr, // 必填，生成签名的随机串
		signature: data.paySign, // 必填，签名，见附录1
		jsApiList: ['chooseWXPay', 'checkJsApi'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
	});

	wx.ready(function() {
		wx.chooseWXPay({
			appId: data.appId,
			timestamp: data
				.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
			nonceStr: data.nonceStr, // 支付签名随机串，不长于 32 位
			package: data.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
			signType: data.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
			paySign: data.paySign, // 支付签名
			success(res) {
				// 支付成功后的回调函数
				uni.showToast({
					title: '支付成功',
					duration: 2000,
					icon: 'none',
				});
				uni.switchTab({
					url: '../../users/user/user'
				})
				// 此处的代码为刷新当前页面
				// var pages = getCurrentPages(); // 获取当前页面栈的实例，以数组形式按栈的顺序给出，第一个元素为首页，最后一个元素为当前页面
				// var curPage = pages[pages.length - 1]; // 当前页面是这个页面栈数组中的最后一个
				// curPage.page = 1;
				// curPage.getinfo(_this.option.uid);
				callback(res);
			},
			fail(res) {
				uni.showToast({
					title: '支付失败',
					duration: 2000,
					icon: 'none',
				});
				errCallback(res);
			}
		});
	});

	wx.error(function(res) {
		// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
		/*alert("config信息验证失败");*/
	});
}
